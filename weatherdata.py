#!/usr/bin/env python3
"""
author: Jantinus Daling
license: GPLv3

John 3:16
“For God so loved the world, that he gave his only begotten Son, 
that whosoever believeth in him should not perish, but have everlasting life.”
"""

# This script is used to collect actual and forecast weatherdata from OpenWeaterMap
# The script runs every hour and safes the data in the respective tables in irrigation.db

from datetime import datetime, timedelta, timezone, date
import os
import json
import linecache
import urllib.request
import sqlalchemy as db
from sqlalchemy import create_engine, ForeignKey, Column, String, Integer, CHAR, MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, close_all_sessions
import schedule
import time

data = declarative_base()

class currentWeather(data):
    __tablename__ = "currentWeather"
    id = Column(Integer, primary_key=True)
    time = Column("time", String)
    temperature = Column("temperature", String)
    pressure = Column("pressure", String)
    minimumTemperature = Column("minimumTemperature", String)
    maximumTemperature = Column("maximumTemperature", String)
    humidity = Column("humidity", String)
    windspeed = Column("windspeed", String)
    rain = Column("rain", String)
    icon = Column("icon", String)

    def __init__(self, time, temperature, pressure, minimumTemperature, maximumTemperature, humidity, windspeed, rain, icon):
        self.time = time
        self.temperature = temperature
        self.pressure = pressure
        self.minimumTemperature = minimumTemperature
        self.maximumTemperature = maximumTemperature
        self.humidity = humidity
        self.windspeed = windspeed
        self.rain = rain
        self.icon = icon

    def __repr__(self):
        return f"({self.time} {self.temperature} {self.pressure} {self.minimumTemperature} {self.maximumTemperature} {self.humidity} {self.windspeed} {self.rain} {self.icon})"

class weatherForecast(data):
    __tablename__ = "weatherForecast"
    id = Column(Integer, primary_key=True)
    time = Column("time", String)
    rain = Column("rain", String)
    minimumTemperature = Column("minimumTemperature", String)
    maximumTemperature = Column("maximumTemperature", String)

    def __init__(self, time, rain, minimumTemperature, maximumTemperature):
        self.time = time
        self.rain = rain
        self.minimumTemperature = minimumTemperature
        self.maximumTemperature = maximumTemperature

    def __repr__(self):
        return f"({self.time} {self.rain} {self.minimumTemperature} {self.maximumTemperature})"

engine = create_engine("sqlite:///irrigation.db", echo=True)
data.metadata.create_all(bind=engine)

Session = sessionmaker(bind=engine)
session = Session()

meta = db.MetaData(bind=engine)
db.MetaData.reflect(meta)

def storeWeatherData():
    # get settings data
    currentTime = datetime.now(timezone.utc)
    settings = meta.tables['general']
    query = db.select(settings.c.location, settings.c.openWeatherKey).where(settings.c.site == '1')
    result = engine.execute(query).fetchall()
    location = str(int(result[0][0]))
    openWeatherAPIKey = result[0][1]

    # get current weather
    openWeatherURL = 'http://api.openweathermap.org/data/2.5/weather?id='+location+'&appid='+openWeatherAPIKey+''
    try:
        weatherdata = urllib.request.urlopen(openWeatherURL)
        data = json.loads(weatherdata.read())

        temperature = int(data['main']['temp'] - 272.15)
        pressure = int(data['main']['pressure'])
        minimumTemperature = int(data['main']['temp_min'] - 272.15)
        maximumTemperature = int(data['main']['temp_max'] - 272.15)
        humidity = int(data['main']['humidity'])
        windspeed = int(data['wind']['speed'] * 3.6)
        try:
            rain = float(data['rain']['1h'])
        except KeyError:
            rain = 0
        icon = data['weather'][0]['icon']

        currentweather = currentWeather(currentTime, temperature, pressure, minimumTemperature, maximumTemperature, humidity, windspeed, rain, icon)
        session.add(currentweather)
        session.commit()
    except:
        print('no weather data available')

    # get weather forecast
    openWeatherForecastURL = 'http://api.openweathermap.org/data/2.5/forecast?id='+location+'&appid='+openWeatherAPIKey+''
    try:
        weatherdata = urllib.request.urlopen(openWeatherForecastURL)
        data = json.loads(weatherdata.read())

        rainForecast = 0
        maximumTemperature = 0
        minimumTemperature = 0
        tempMaxList = []
        tempMinList = []
        for x in range(1,9):
            try:
                rain = data['list'][x]['rain']['3h']
            except KeyError:
                rain = 0
            rainForecast = rainForecast + int(rain)
            try:
                tempMax = data['list'][x]['main']['temp_max']
            except KeyError:
                tempMax = 0
            tempMaxList.append(tempMax)
            try:
                tempMin = data['list'][x]['main']['temp_min']
            except KeyError:
                tempMin = 0
            tempMinList.append(tempMin)

        maximumTemperature = max(tempMaxList) - 272.15
        minimumTemperature = min(tempMinList) - 272.15

        weatherforecast = weatherForecast(currentTime, rainForecast, minimumTemperature, maximumTemperature)
        session.add(weatherforecast)
        session.commit()
    except:
        print('no weather data available')

schedule.every().hour.do(storeWeatherData)

while True:
    schedule.run_pending()
    time.sleep(1)