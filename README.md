Openirrigation is open source irrigation software that runs on a raspberry pi.
After configuration the software will download weather data from openweathermap and calculate water usage based on plant growth, temperaturedata and raindata.

The program creates a virtual buffer that fills by irrigation when the buffer gets to low.
The goal is to maximize pauses between irrigation forcing strong root growth and optimize the use of rainfall.

After succesfull installation of the raspberry pi: https://www.raspberrypi.com/software/ login to the shell via ssh.
Run the following commands:

cd /tmp 
wget https://gitlab.com/jtdaling/openirrigation/-/blob/master/install.sh
sudo ./install.sh 

After the installation has finished visit the url of your raspberry pi and finish all the settings.
