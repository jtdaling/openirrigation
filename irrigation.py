#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  5 16:59:17 2018

@author: jantinus
"""

# This script calculates the amount of water absorbed and stores the data every hour
# If the waterbuffer is below 24mm the sprinkler irrigation will run at 0300, 0500 and 0700 local time
# The drip irrigation will run all at once during daytime.

from datetime import datetime, timedelta, timezone, date
import sqlalchemy as db
from sqlalchemy import create_engine, ForeignKey, Column, String, Integer, CHAR, MetaData, func, update
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, close_all_sessions
import schedule
import time
import math
import RPi.GPIO as GPIO

def resetGPIOPins():
    for i in range(1,9):
        try:
            readData = irrigationDB.execute("SELECT gpio FROM valveData WHERE valveID = ?", i)
            gpio = int(readData.fetchone()[0])
            GPIO.setwarnings(False)
            GPIO.setmode(GPIO.BCM)
            GPIO.setup(gpio, GPIO.OUT)
            GPIO.output(gpio, GPIO.HIGH)
        except:
            'no gpio pin defined'


data = declarative_base()

class irrigationData(data):
    __tablename__ = "irrigationData"
    id = Column(Integer, primary_key=True)
    plantID = Column("plantID", Integer)
    time = Column("time", String)
    waterBuffer = Column("waterBuffer", String)
    irrigation = Column("irrigation", String)
    rain = Column("rain", String)

    def __init__(self, plantID, time, waterBuffer, irrigation, rain):
        self.plantID = plantID
        self.time = time
        self.waterBuffer = waterBuffer
        self.irrigation = irrigation
        self.rain = rain

    def __repr__(self):
        return f"({self.plantID} {self.time} {self.waterBuffer} {self.irrigation} {self.rain})"

irrigationDB = create_engine("sqlite:///irrigation.db", echo=True)
data.metadata.create_all(bind=irrigationDB)

Session = sessionmaker(bind=irrigationDB)
session = Session()

meta = db.MetaData(bind=irrigationDB)
db.MetaData.reflect(meta)

plantDB = create_engine("sqlite:///plantdata.db", echo=True)
#plantMeta = MetaData(bind=plantDB)
#PlantData.reflect(plantMeta)


def getPlantData(plantName):
    print(plantName)
    readData = plantDB.execute("SELECT * FROM PlantInfo WHERE PlantName = ?", plantName)
    try:
        result = readData.fetchmany(1)
        minTemperature = result[0][2]
        maxTemperature = result[0][3]
        optimumTemperature = (int(result[0][2]) + int(result[0][3])) /2
        waterPerSeason = result[0][5]
    except:
        minTemperature = 15
        maxTemperature = 25
        optimumTemperature = 20
        waterPerSeason = 2000
    return minTemperature, maxTemperature, optimumTemperature, waterPerSeason

def getValveSettings(valveID):
    readData = irrigationDB.execute("SELECT valve, plantType, valvecorrection FROM valveData WHERE valveID = ?", valveID)
    result = readData.fetchmany(1)
    return result

def getWeatherData():
    try:
        readData = irrigationDB.execute("SELECT max(id) FROM currentWeather")
        lastRowId = readData.fetchmany(1)[0][0]
        readData = irrigationDB.execute("SELECT minimumTemperature, maximumTemperature, windspeed, rain FROM currentWeather WHERE id = ?", lastRowId)
        weatherData = readData.fetchmany(1)
        averageTemperature = (int(weatherData[0][0]) + int(weatherData[0][1])) /2
        windspeed = weatherData[0][2]
        rain = weatherData[0][3]
    except:
        averageTemperature = None
        windspeed = None
        rain = None
    return(averageTemperature, windspeed, rain)

def calculateWaterBufferHourly():
    weatherData = getWeatherData()
    for i in range(1,9):
        try:
            valveData = getValveSettings(i)
            valveActive = valveData[0][0]
        except:
            valveActive = None
        if valveActive == "on":
            plantData = getPlantData(valveData[0][1])
            correction = int(valveData[0][2])*0.01
            maxWaterConsumptionPerHour = int(plantData[3])/52/7/24
            if int(weatherData[0]) < int(plantData[2]):
                tCalc1 = 1 - (float(plantData[2]) - float(weatherData[0])) / float(float(plantData[2]) - float(plantData[0]))
                if tCalc1 <= 0:
                    tCalc = 0
                else:
                    tCalc = tCalc1
            if int(weatherData[0]) > int(plantData[2]):
                tCalc1 = 1 - (float(weatherData[0]) - float(plantData[2])) / float(float(plantData[2]) - float(plantData[0]))
                if tCalc1 <= 0.8:
                    tCalc = 0.8
                else:
                    tCalc = tCalc1
            if int(weatherData[0]) == int(plantData[2]):
                tCalc = 1
            hourlyWaterConsumption = ((math.sin(1.570795 * tCalc)) * maxWaterConsumptionPerHour) * correction
            readData = irrigationDB.execute("SELECT max(id) FROM irrigationData WHERE plantID = ?", i)
            rowID = readData.fetchmany(1)[0][0]
            readData = irrigationDB.execute("SELECT waterBuffer FROM irrigationData WHERE id = ?", rowID)
            try:
                previousWaterBuffer = float(readData.fetchone()[0])
            except:
                if valveData[0][1] == "Pots":
                    previousWaterBuffer = 8
                else:
                    previousWaterBuffer = 24
            if valveData[0][1] == "Pots":
                waterBuffer = previousWaterBuffer - hourlyWaterConsumption
            else:
                waterBuffer = (previousWaterBuffer - hourlyWaterConsumption) + float(weatherData[2])
            # max waterbuffer 28, if it rains more it will likely flow away
            if waterBuffer > 28:
                waterBuffer = 28
            if rowID == None:
                plantID = i
                currentTime = datetime.now(timezone.utc)
                irrigation = 0
                rain = weatherData[2]
                irrigationdata = irrigationData(plantID, currentTime, waterBuffer, irrigation, rain)
                session.add(irrigationdata)
                session.commit()
            else:
                irrigationdata = meta.tables['irrigationData']
                updateWaterBuffer = update(irrigationdata)
                updateWaterBuffer = updateWaterBuffer.values({"waterBuffer": waterBuffer})
                updateWaterBuffer = updateWaterBuffer.where(irrigationdata.c.id == rowID)
                session.execute(updateWaterBuffer)
                session.commit()

def Sprinklerirrigation():
    resetGPIOPins()
    waterBuffers = []
    irrigationStart = datetime.now()
    for i in range(1,9):
        waterBuffers.append(24)
        readData = irrigationDB.execute("SELECT valve, valveOutput, maxWind, irrigationType FROM valveData WHERE valveID=?", i)
        valveData = readData.fetchmany(1)
        try:
            if valveData[0][0] == "on" and valveData[0][3] == "Sprinkler":
                readData = irrigationDB.execute("SELECT max(id) FROM irrigationData WHERE plantID=?", i)
                lastRowId = readData.fetchmany(1)[0][0]
                readData = irrigationDB.execute("SELECT waterBuffer FROM irrigationData WHERE id = ?", lastRowId)
                waterBuffer = float(readData.fetchone()[0])
                waterBuffers[i-1] = waterBuffer
            if waterBuffer <= 2:
                readData = irrigationDB.execute("SELECT max(id) FROM currentWeather")
                lastRowId = readData.fetchone()[0]
                readData = irrigationDB.execute("SELECT windspeed FROM currentWeather WHERE id = ?", lastRowId)
                currentWindSpeed = int(readData.fetchone()[0])
                print("zone: ", i)
                if valveData[0][2] == 0 or valveData[0][2] >= currentWindSpeed and int(valveData[0][1]) > 0:
                    irrigationTime = 8 / int(valveData[0][1])
                    startTime = datetime.now()
                    readData = irrigationDB.execute("SELECT gpio FROM valveData WHERE valveID = ?", i)
                    gpio = int(readData.fetchone()[0])
                    GPIO.output(gpio, GPIO.LOW)
                    time.sleep(float(irrigationTime * 3600))
                    GPIO.output(gpio, GPIO.HIGH)
                    endTime = datetime.now()
                    irrigationamount = round(((endTime - startTime).seconds * float(valveData[0][1]) / 3600), 1)
                    currentTime = datetime.now(timezone.utc)
                    rain = 0
                    currentIrrigation = irrigationData(i, currentTime, waterBuffer + irrigationamount, irrigationamount, rain)
                    session.add(currentIrrigation)
                    session.commit()
        except:
            'no irrigation scheduled'
    if min(waterBuffers) <= 2:
        for j in range(2):
            while True:
                # wait 1 hour since irrigation start
                if (1*3600 - (datetime.now() - irrigationStart).seconds) < 0:
                    irrigationStart = datetime.now()
                    for i in range(1,9):
                        readData = irrigationDB.execute("SELECT valve, valveOutput, maxWind, irrigationType FROM valveData WHERE valveID=?", i)
                        valveData = readData.fetchmany(1)
                        try:
                            if valveData[0][0] == "on" and valveData[0][3] == "Sprinkler":
                                readData = irrigationDB.execute("SELECT max(id) FROM irrigationData WHERE plantID=?", i)
                                lastRowId = readData.fetchmany(1)[0][0]
                                readData = irrigationDB.execute("SELECT waterBuffer FROM irrigationData WHERE id = ?", lastRowId)
                                try:
                                    waterBuffer = float(readData.fetchone()[0])
                                except:
                                    waterBuffer = 24
                                if waterBuffers[i-1] <= 2:
                                    readData = irrigationDB.execute("SELECT max(id) FROM currentWeather")
                                    lastRowId = readData.fetchone()[0]
                                    readData = irrigationDB.execute("SELECT windspeed FROM currentWeather WHERE id = ?", lastRowId)
                                    currentWindSpeed = int(readData.fetchone()[0])
                                    print("Zone: ", i)
                                    if valveData[0][2] == 0 or valveData[0][2] > currentWindSpeed and int(valveData[0][1]) > 0:
                                        if 24 - waterBuffer > 8:
                                            irrigationTime = 8 / int(valveData[0][1])
                                        else:
                                            irrigationTime = (24 - waterBuffer) / int(valveData[0][1])
                                        startTime = datetime.now()
                                        readData = irrigationDB.execute("SELECT gpio FROM valveData WHERE valveID = ?", i)
                                        gpio = int(readData.fetchone()[0])
                                        GPIO.output(gpio, GPIO.LOW)
                                        time.sleep(float(irrigationTime * 3600))
                                        GPIO.output(gpio, GPIO.HIGH)
                                        endTime = datetime.now()
                                        irrigationamount = round(((endTime - startTime).seconds * float(valveData[0][1]) / 3600), 1)
                                        currentTime = datetime.now(timezone.utc)
                                        rain = 0
                                        currentIrrigation = irrigationData(i, currentTime, waterBuffer + irrigationamount, irrigationamount, rain)
                                        session.add(currentIrrigation)
                                        session.commit()
                        except:
                            'no irrigation scheduled'
                    break
                else:
                    time.sleep(10)

def Dripirrigation():
    resetGPIOPins()
    print("starting")
    waterBuffers = []
    irrigationStart = datetime.now()
    for i in range(1,9):
        waterBuffers.append(24)
        readData = irrigationDB.execute("SELECT valve, valveOutput, maxWind, irrigationType, plantType FROM valveData WHERE valveID=?", i)
        valveData = readData.fetchmany(1)
        try:
            if valveData[0][0] == "on" and valveData[0][3] == "Drip":
                readData = irrigationDB.execute("SELECT max(id) FROM irrigationData WHERE plantID=?", i)
                lastRowId = readData.fetchmany(1)[0][0]
                readData = irrigationDB.execute("SELECT waterBuffer FROM irrigationData WHERE id = ?", lastRowId)
                try:
                    waterBuffer = float(readData.fetchone()[0])
                except:
                    print(valveData[0][4])
                    if valveData[0][4] == "Pots":
                        waterBuffer = 8
                    else:
                        waterBuffer = 24
                waterBuffers[i-1] = waterBuffer
                if waterBuffer < 2:
                    readData = irrigationDB.execute("SELECT max(id) FROM currentWeather")
                    lastRowId = readData.fetchone()[0]
                    readData = irrigationDB.execute("SELECT windspeed FROM currentWeather WHERE id = ?", lastRowId)
                    currentWindSpeed = int(readData.fetchone()[0])
                    if valveData[0][2] == 0 or valveData[0][2] > currentWindSpeed and int(valveData[0][1]) > 0:
                        if valveData[0][4] == "Pots":
                            irrigationTime = (8 - waterBuffer) / int(valveData[0][1])
                        else:
                            irrigationTime = (24 - waterBuffer) / int(valveData[0][1])
                        startTime = datetime.now()
                        readData = irrigationDB.execute("SELECT gpio FROM valveData WHERE valveID = ?", i)
                        gpio = int(readData.fetchone()[0])
                        GPIO.output(gpio, GPIO.LOW)
                        # allow time for hoses to fill up
                        time.sleep(30)
                        time.sleep(float(irrigationTime * 3600))
                        GPIO.output(gpio, GPIO.HIGH)
                        endTime = datetime.now()
                        irrigationamount = round(((endTime - startTime).seconds * float(valveData[0][1]) / 3600), 1)
                        currentTime = datetime.now(timezone.utc)
                        rain = 0
                        currentIrrigation = irrigationData(i, currentTime, waterBuffer + irrigationamount, irrigationamount, rain)
                        session.add(currentIrrigation)
                        session.commit()
        except:
            'no irrigation needed'

resetGPIOPins()

schedule.every().hour.do(calculateWaterBufferHourly)
schedule.every().day.at("03:00").do(Sprinklerirrigation)
schedule.every().day.at("11:00").do(Dripirrigation)

while True:
    schedule.run_pending()
    time.sleep(1)
