#!/usr/bin/env python3
"""
author: Jantinus Daling
license: GPLv3

John 3:16
“For God so loved the world, that he gave his only begotten Son, 
that whosoever believeth in him should not perish, but have everlasting life.”
"""


from flask import Flask, request, render_template, redirect
from models import UserModel, db, login
from flask_login import login_required, current_user, login_user, logout_user

# from sqlalchemy.engine import result
# import sqlalchemy
# from sqlalchemy import create_engine, ForeignKey, Table, Column, Numeric, String, Integer, VARCHAR, CHAR, MetaData, func, delete, insert, update
from sqlalchemy.ext.declarative import declarative_base
# from sqlalchemy.orm import sessionmaker
from sqlalchemy.engine import result
import sqlalchemy
from sqlalchemy import create_engine, MetaData, Table, Column, Numeric, String, Integer, VARCHAR, update, delete
from datetime import datetime

import os

app =Flask(__name__)
app.secret_key = '8Hf4$sd9!1Mn0321#'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///userdata.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db.init_app(app)
@app.before_first_request
def create_table():
    db.create_all()

login.init_app(app)
login.login_view = 'login'

# set variables
version = "2.2"

data = declarative_base()

class generalData(data):
    __tablename__ = "general"
    site = Column("site", Integer, primary_key=True)
    unit = Column("unit", Integer)
    waterpump = Column("waterpump", Integer)
    timeZone = Column("timeZone", String)
    location = Column("location", String)
    openWeatherKey = Column("openWeatherKey", String)
    updateURL = Column("updateURL", String)
    def __init__(self, unit, waterpump, timeZone, location, openWeatherKey, updateURL):
        self.unit = unit
        self.waterpump = waterpump
        self.timeZone = timeZone
        self.location = location
        self.openWeatherKey = openWeatherKey
        self.updateURL = updateURL

    def __repr__(self):
        return f"({self.site} {self.unit} {self.waterpump} {self.timeZone} {self.location} {self.openWeatherKey} {self.updateURL})"

class currentWeather(data):
    __tablename__ = 'currentWeather'
    id = Column(Integer, primary_key=True)
    time = Column(String)
    temperature = Column(String)
    pressure = Column(String)
    minimumTemperature = Column(String)
    maximumTemperature = Column(String)
    humidity = Column(String)
    windspeed = Column(String)
    rain = Column(String)
    icon = Column(String)


engine = create_engine("sqlite:///irrigation.db", echo=True)
meta = MetaData(bind=engine)
MetaData.reflect(meta)

plantData = create_engine("sqlite:///plantdata.db", echo=True)


def getGeneralData():
    readData = engine.execute("SELECT * FROM general")
    result = readData.fetchmany(1)
    unit = result[0][1]
    waterpump = result[0][2]
    timeZone = result[0][3]
    location = result[0][4]
    openWeatherKey = result[0][5]
    updateURL = result[0][6]
    try:
        readData = engine.execute("SELECT max(id) FROM currentWeather")
        lastRowId = readData.fetchmany(1)[0][0]
        icon = engine.execute("SELECT icon FROM currentWeather WHERE id = ?", lastRowId)
        icon = icon.fetchone()[0]
    except:
        icon = None
    return unit, waterpump, timeZone, location, openWeatherKey, icon, updateURL

def getValveData(valveID):
    readData = engine.execute("SELECT * FROM valveData WHERE valveID = ?", valveID)
    result = readData.fetchmany(1)
    valve = result[0][1]
    valveName = result[0][2]
    valveOutput = result[0][3]
    valveCorrection = result[0][4]
    maxWind = result[0][5]
    plantType = result[0][6]
    irrigationType = result[0][7]
    gpio = result[0][8]
    try:
        readData = engine.execute("SELECT max(id) FROM currentWeather")
        lastRowId = readData.fetchmany(1)[0][0]
        icon = engine.execute("SELECT icon FROM currentWeather WHERE id = ?", lastRowId)
        icon = icon.fetchone()[0]
    except:
        icon = None
    return valve, valveName, valveOutput, valveCorrection, maxWind, plantType, irrigationType, gpio

def mainOverviewData():
    waterBuffer = []
    lastIrrigation = []
    lastIrrigationAmount = []
    valveSwitch = []
    valveNames = []
    plantSorts = []
    valveCorrections = []
    growthConditions = []
    monthlyIrrigations = []
    monthlyIrrigationsPercent = []
    try:
        readData = engine.execute("SELECT max(id) FROM currentWeather")
        lastRowId = readData.fetchmany(1)[0][0]
        readData = engine.execute("SELECT temperature, humidity, icon FROM currentWeather WHERE id = ?", lastRowId)
        result = readData.fetchmany(1)
        temperature = result[0][0]
        humidity = result[0][1]
        icon = result[0][2]
    except:
        temperature = humidity = icon = None
    for i in range(1,9):
        readData = engine.execute("SELECT max(id) FROM irrigationData WHERE plantID=?", i)
        lastRowId = readData.fetchmany(1)[0][0]
        readData = engine.execute("SELECT waterBuffer FROM irrigationData WHERE id = ?", lastRowId)
        try:
            result = readData.fetchone()[0]
        except:
            result = None
        waterBuffer.append(result)
        readData = engine.execute("SELECT max(id) FROM irrigationData WHERE plantID=? AND irrigation != 0", i)
        try:
            lastRowId = readData.fetchmany(1)[0][0]
            readData = engine.execute("SELECT time FROM irrigationData WHERE id = ?", lastRowId)
            result = readData.fetchone()[0]
        except:
            result = None
        if result != None:
            result = datetime.strptime(result[0:10], "%Y-%m-%d")
            lastIrrigation.append(result.strftime("%b %d"))
        else:
            lastIrrigation.append(result)
        try:
            readData = engine.execute(" SELECT SUM(irrigation) FROM irrigationData WHERE time >= date(?) AND plantID = ?", result, i)
            result = readData.fetchone()[0]
        except:
            result = None
        if result == None:
            result = 0
        lastIrrigationAmount.append(result)
        try:
            readData = engine.execute("SELECT valve, valveName, valveCorrection, plantType FROM valveData WHERE valveID = ?", i)
            result = readData.fetchmany(1)
            valve = result[0][0]
            valveName = result[0][1]
            valveCorrection = result[0][2]
            plantType = result[0][3]
            if valve == "on":
                valveCorrections.append(valveCorrection)
                readData = plantData.execute("SELECT minTemperature, maxTemperature, WaterPerSeason FROM PlantInfo WHERE PlantName = ?", plantType)
                result = readData.fetchmany(1)
                minTemperature = result[0][0]
                maxTemperature = result[0][1]
                temperatureGrowthInfluence = (int(temperature) / (int(maxTemperature) - int(minTemperature)))
                growthConditions.append(temperatureGrowthInfluence)
        except:
            valve = valveName = valveCorrection = plantType = None
        plantSorts.append(plantType)
        valveSwitch.append(valve)
        valveNames.append(valveName)
        valveEfficiency = int(sum(valveCorrections) / len(valveCorrections))
        currentYear = str(datetime.now().year)
        readData = engine.execute(" SELECT SUM(rain) FROM currentWeather WHERE time >= date('now', '-24 hours');")
        try:
            rainPast24Hours = int(readData.fetchone()[0])
        except:
            rainPast24Hours = 0
        readData = engine.execute("SELECT SUM(rain) FROM irrigationData WHERE STRFTIME('%Y', time) = ?", currentYear)
        try:
            totalRainThisYear = readData.fetchone()[0]
        except:
            totalRainThisYear = 0
        readData = engine.execute("SELECT SUM(irrigation) FROM irrigationData WHERE STRFTIME('%Y', time) = ?", currentYear)
        try:
            totalIrrigationThisYear = readData.fetchone()[0]
        except:
            totalIrrigationThisYear = 0
        try:
            irrigationDependency = int((totalIrrigationThisYear / (totalIrrigationThisYear + totalRainThisYear))*100)
            if irrigationDependency > 100:
                irrigationDependency = 100
        except:
            irrigationDependency = 0
    for i in range(1,13):
        if i < 10:
            x = str(i).zfill(2)
        else:
            x = str(i)
        readData = engine.execute("SELECT SUM(irrigation) FROM irrigationData WHERE STRFTIME('%m', time) = ?", x)
        irrigationThisMonth = readData.fetchone()[0]
        readData = engine.execute("SELECT COUNT(irrigation) FROM irrigationData WHERE STRFTIME('%m', time) = ?", x)
        numberOfIrrigationsThisMonth = readData.fetchone()[0]
        if irrigationThisMonth == None:
            irrigationThisMonth = 0
        if irrigationThisMonth == 0:
            monthlyIrrigations.append(int(irrigationThisMonth))
            monthlyIrrigationsPercent.append(irrigationThisMonth)
        else:
            monthlyIrrigations.append(int(irrigationThisMonth/numberOfIrrigationsThisMonth))
            monthlyIrrigationsPercent.append(irrigationThisMonth/numberOfIrrigationsThisMonth)
    maxMonthlyIrrigations = max(monthlyIrrigations)
    try:
        for i in range(0, len(monthlyIrrigations)):
            monthlyIrrigationsPercent[i] = (monthlyIrrigations[i] / maxMonthlyIrrigations)*100
    except:
        'no irrigations completed'
    try:
        growthCondition = int((sum(growthConditions) / len(growthConditions))*100)
    except:
        growthCondition = 0
    today = str(datetime.today().strftime('%Y-%m-%d'))
    readData = engine.execute("SELECT COUNT(DISTINCT plantID) FROM irrigationData WHERE irrigation != 0 AND STRFTIME('%Y-%m-%d', time) = ?", today)
    irrigationsToday = readData.fetchone()[0]

    return temperature, humidity, icon, waterBuffer, lastIrrigation, valveSwitch, valveNames, valveEfficiency, growthCondition, irrigationDependency, monthlyIrrigations, irrigationsToday, maxMonthlyIrrigations, lastIrrigationAmount, monthlyIrrigationsPercent, rainPast24Hours, plantSorts

@app.route('/updateSoftware')
def updateSoftware():
    readData = engine.execute("SELECT updateURL FROM general")
    try:
        updateURL = readData.fetchone()[0]
    except:
        updateURL = None
    if updateURL != None:
        os.system('rm -rf /tmp/openirrigation')
        updateURL = 'git clone ' + updateURL + ' /tmp/openirrigation'
        os.system(updateURL)
        os.system('cp -R /tmp/openirrigation/* /opt/openirrigation')
        os.system('rm -rf /tmp/openirrigation')
        os.system('systemctl restart openirrigation.service')
        os.system('systemctl restart weatherdata.service')
    return redirect('/')

def get_locale():
    supported_languages = ["en", "de", "nl"]
    return request.accept_languages.best_match(supported_languages)

@app.route('/removeUser')
@login_required
def removeUser():
    username = current_user.username
    remove_user = request.args.get('remove_user')
    if remove_user == username:
        return redirect('/user')
    else:
        #UserModel.query.filter_by(username=remove_user).delete()
        user = UserModel.query.filter_by(username=remove_user).first()
        # print(user)
        db.session.delete(user)
        db.session.commit()
        return redirect('/')

@app.route('/', methods=["GET","POST"])
@login_required
def index():
    username = current_user.username
    overviewSelected = "Irrigation"
    try:
        unit, waterpump, timeZone, location, openWeatherKey, icon, updateURL = getGeneralData()
    except:
        # first start; finish settings
        return redirect('/settings')
    temperature, humidity, icon, waterBuffer, lastIrrigation, valveSwitch, valveNames, valveEfficiency, growthCondition, irrigationDependency, monthlyIrrigations, irrigationsToday, maxMonthlyIrrigations, lastIrrigationAmount, monthlyIrrigationsPercent, rainPast24Hours, plantSorts = mainOverviewData()
    if request.method == 'POST':
        menuSelected = request.form.get('overviewSelected')
        if menuSelected is not None:
            overviewSelected = menuSelected
    return render_template('index.html', username=username, icon=icon, location=location, overviewSelected=overviewSelected,
        temperature=temperature, humidity=humidity, waterBuffer=waterBuffer, lastIrrigation=lastIrrigation, lastIrrigationAmount=lastIrrigationAmount,
        valveSwitch=valveSwitch, valveNames=valveNames, valveEfficiency=valveEfficiency, growthCondition=growthCondition,
        irrigationDependency=irrigationDependency, monthlyIrrigations=monthlyIrrigations, irrigationsToday=irrigationsToday,
        maxMonthlyIrrigations=maxMonthlyIrrigations, monthlyIrrigationsPercent=monthlyIrrigationsPercent, rainPast24Hours=rainPast24Hours, plantSorts=plantSorts)

@app.route('/login', methods = ['POST', 'GET'])
def login():
    if current_user.is_authenticated:
        return redirect('/')
    else:
        results = UserModel.query.all()
        counter = 0
        for user in results:
            counter += 1
        if counter == 0:
            return redirect('/register')
        if request.method == 'POST':
            email = request.form['email']
            user = UserModel.query.filter_by(email = email).first()
            if user is not None and user.check_password(request.form['password']):
                login_user(user)
                return redirect('/')

    return render_template('login.html')

@app.route('/register', methods=['POST', 'GET'])
def register():
    results = UserModel.query.all()
    counter = 0
    for user in results:
        counter += 1
    if counter == 0:
        if request.method == 'POST':
            email = request.form['email']
            username = request.form['username']
            password = request.form['password']
            if UserModel.query.filter_by(email=email).first():
                return ('Email already Present')
            user = UserModel(email=email, username=username)
            user.set_password(password)
            db.session.add(user)
            db.session.commit()
            return redirect('/user')
        return render_template('register.html')
    else:
        return redirect('/')

@app.route('/user', methods=['POST', 'GET'])
def user():
    if current_user.is_authenticated:
        username = current_user.username
        settingsSelected = "user"
        unit, waterpump, timeZone, location, openWeatherKey, icon = getGeneralData()
        users = UserModel.query.all()
        if request.method == 'POST':
            menuSelected = request.form.get('settingsSelected')
            if menuSelected is not None:
                settingsSelected = menuSelected
                if menuSelected == "general":
                    unit, waterpump, timeZone, location, openWeatherKey, icon = getGeneralData()
                    return render_template('settings.html', username=username, icon=icon, location=location, settingsSelected=settingsSelected,
                        unit=unit, waterpump=waterpump, timeZone=timeZone, openWeatherKey=openWeatherKey)
                else:
                    valveID = settingsSelected.partition("Zone")[2]
                    try:
                        valve, valveName, valveOutput, valveCorrection, maxWind, plantType, irrigationType, gpio = getValveData(valveID)
                    except:
                        valve = "on"
                        valveCorrection = 100
                        maxWind = 0
                        valveName, valveOutput, plantType, irrigationType  = ("",)*5
                    return render_template('settings.html', username=username, icon=icon, settingsSelected=settingsSelected, valve=valve,
                        valveName=valveName, valveOutput=valveOutput, valveCorrection=valveCorrection, maxWind=maxWind, plantType=plantType,
                        irrigationType=irrigationType, gpio=gpio)
            else:
                email = request.form['email']
                username = request.form['username']
                password = request.form['password']
                if UserModel.query.filter_by(email=email).first():
                    return ('Email already Present')
                user = UserModel(email=email, username=username)
                user.set_password(password)
                db.session.add(user)
                db.session.commit()
                return redirect('/user')
    else:
        # if more than one user do not allow registration by new user
        results = UserModel.query.all()
        counter = 0
        for user in results:
            counter += 1
        if counter != 0:
            return redirect('/login')
        else:
            if request.method == 'POST':
                email = request.form['email']
                username = request.form['username']
                password = request.form['password']
                if UserModel.query.filter_by(email=email).first():
                    return ('Email already Present')
                user = UserModel(email=email, username=username)
                user.set_password(password)
                db.session.add(user)
                db.session.commit()
                return redirect('/login')
    return render_template('user.html', username=username, icon=icon, location=location, settingsSelected=settingsSelected, users=users)

@app.route('/logout')
def logout():
    logout_user()
    return redirect('/')

@app.route('/settings', methods=["GET","POST"])
@login_required
def settings():
    username = current_user.username
    settingsSelected = "general"
    try:
        unit, waterpump, timeZone, location, openWeatherKey, icon, updateURL = getGeneralData()
    except:
        unit = waterpump = timeZone = location = openWeatherKey = icon = updateURL = None
    if request.method == 'POST':
        menuSelected = request.form.get('settingsSelected')
        if menuSelected is not None:
            settingsSelected = menuSelected
            if menuSelected == "general":
                unit, waterpump, timeZone, location, openWeatherKey, icon, updateURL = getGeneralData()
                return render_template('settings.html', username=username, icon=icon, location=location, settingsSelected=settingsSelected,
                    unit=unit, waterpump=waterpump, timeZone=timeZone, openWeatherKey=openWeatherKey, updateURL=updateURL)
            else:
                valveID = settingsSelected.partition("Zone")[2]
                try:
                    valve, valveName, valveOutput, valveCorrection, maxWind, plantType, irrigationType, gpio = getValveData(valveID)
                except:
                    valve = "on"
                    valveCorrection = 100
                    maxWind = 0
                    valveName, valveOutput, plantType, irrigationType, gpio  = ("",)*5
                return render_template('settings.html', username=username, icon=icon, settingsSelected=settingsSelected, valve=valve,
                    valveName=valveName, valveOutput=valveOutput, valveCorrection=valveCorrection, maxWind=maxWind, plantType=plantType,
                    irrigationType=irrigationType, gpio=gpio)
        else:
            updateURL = request.form.get('updateURL')
            unit = request.form.get('unit')
            waterpump = request.form.get('pump')
            timeZone = request.form.get('timeZone')
            location = request.form.get('location')
            openWeatherKey = request.form.get('openWeatherKey')
            valveSelected = request.form.get('valveSelected')
            valve = request.form.get('valve')
            valveName = request.form.get('valveName')
            valveOutput = request.form.get('valveOutput')
            valveCorrection = request.form.get('valveCorrection')
            maxWind = request.form.get('maxWind')
            plantType = request.form.get('plantType')
            irrigationType = request.form.get('irrigationType')
            gpio = request.form.get('gpio')
            if openWeatherKey is not None:
                # store data to excisting table or create new with site id 1
                try:
                    generalData = meta.tables['general']
                    updateData = update(generalData)
                    updateData = updateData.values({"unit": unit, "waterpump": waterpump, "timeZone": timeZone,
                        "location": location, "openWeatherKey": openWeatherKey, "updateURL": updateURL})
                    updateData = updateData.where(generalData.c.site == 1)
                    engine.execute(updateData)
                except:
                    general = Table(
                        'general', meta,
                        Column("site", Integer, primary_key=True),
                        Column("unit", VARCHAR),
                        Column("waterpump", VARCHAR),
                        Column("timeZone", VARCHAR),
                        Column("location", Numeric),
                        Column("openWeatherKey", String),
                        Column("updateURL", String)
                    )
                    meta.create_all(engine)
                    generalData = meta.tables['general']
                    updateData = update(generalData)
                    insertData = general.insert().values({"unit": unit, "waterpump": waterpump, "timeZone": timeZone,
                        "location": location, "openWeatherKey": openWeatherKey, "updateURL": updateURL})
                    engine.execute(insertData)
            if valveName is not None:
                try:
                    updateValveData = meta.tables['valveData']
                    updateData = update(updateValveData)
                    updateData = updateData.values({"valve": valve, "valveName": valveName, "valveOutput": valveOutput,
                        "valveCorrection": valveCorrection, "maxWind": maxWind, "plantType":plantType,
                        "irrigationType":irrigationType, "gpio":gpio})
                    valveID = valveSelected.partition("Zone")[2]
                    checkInDatabase = engine.execute("SELECT valveID FROM valveData WHERE valveID = ?", valveID)
                    try:
                        inDatabase = checkInDatabase.fetchmany(1)[0][0]
                    except:
                        inDatabase = False
                    if inDatabase != False:
                        updateData = updateData.where(updateValveData.c.valveID == valveID)
                        engine.execute(updateData)
                    else:
                        insertData = updateValveData.insert().values({"valveID": valveID, "valve": valve, "valveName": valveName, "valveOutput": valveOutput,
                        "valveCorrection": valveCorrection, "maxWind": maxWind, "plantType":plantType, "irrigationType":irrigationType, "gpio":gpio})
                        engine.execute(insertData)
                except:
                    valveID = valveSelected.partition("Zone")[2]
                    valveData = Table(
                        'valveData', meta,
                        Column("valveID", Integer, primary_key=True),
                        Column("valve", VARCHAR),
                        Column("valveName", VARCHAR),
                        Column("valveOutput", Numeric),
                        Column("valveCorrection", Numeric),
                        Column("maxWind", Numeric),
                        Column("plantType", VARCHAR),
                        Column("irrigationType", VARCHAR),
                        Column("gpio", Numeric)
                    )
                    meta.create_all(engine)
                    valveData = meta.tables['valveData']
                    updateData = update(valveData)
                    insertData = valveData.insert().values({"valve": valve, "valveName": valveName, "valveOutput": valveOutput,
                        "valveCorrection": valveCorrection, "maxWind": maxWind, "plantType":plantType, "irrigationType":irrigationType, "gpio":gpio})
                    engine.execute(insertData)

                valve, valveName, valveOutput, valveCorrection, maxWind, plantType, irrigationType, gpio = getValveData(valveID)
                return render_template('settings.html', username=username, icon=icon, settingsSelected=valveSelected, valve=valve,
                    valveName=valveName, valveOutput=valveOutput, valveCorrection=valveCorrection, maxWind=maxWind, plantType=plantType,
                    irrigationType=irrigationType, gpio=gpio)


    return render_template('settings.html', username=username, icon=icon, location=location, settingsSelected=settingsSelected,
        unit=unit, waterpump=waterpump, timeZone=timeZone, openWeatherKey=openWeatherKey, updateURL=updateURL)

@app.route('/mobile', methods=['POST', 'GET'])
@login_required
def mobile():
    if current_user.is_authenticated:
        username = current_user.username
    else:
        username = "Guest"
    try:
        unit, waterpump, timeZone, location, openWeatherKey, icon, updateURL = getGeneralData()
    except:
        # first start; finish settings
        return redirect('/settings')
    temperature, humidity, icon, waterBuffer, lastIrrigation, valveSwitch, valveNames, valveEfficiency, growthCondition, irrigationDependency, monthlyIrrigations, irrigationsToday, maxMonthlyIrrigations, lastIrrigationAmount, monthlyIrrigationsPercent, rainPast24Hours, plantSorts = mainOverviewData()
    if request.method == 'POST':
        menuSelected = request.form.get('overviewSelected')
        if menuSelected is not None:
            overviewSelected = menuSelected
    return render_template('mobile.html', username=username, icon=icon, location=location,
        temperature=temperature, humidity=humidity, waterBuffer=waterBuffer, lastIrrigation=lastIrrigation, lastIrrigationAmount=lastIrrigationAmount,
        valveSwitch=valveSwitch, valveNames=valveNames, valveEfficiency=valveEfficiency, growthCondition=growthCondition,
        irrigationDependency=irrigationDependency, monthlyIrrigations=monthlyIrrigations, irrigationsToday=irrigationsToday,
        maxMonthlyIrrigations=maxMonthlyIrrigations, monthlyIrrigationsPercent=monthlyIrrigationsPercent, rainPast24Hours=rainPast24Hours, plantSorts=plantSorts)


app.run(host="0.0.0.0", port=80, threaded=False, debug=True)
